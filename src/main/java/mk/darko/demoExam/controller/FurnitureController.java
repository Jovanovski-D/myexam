package mk.darko.demoExam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mk.darko.demoExam.model.Furniture;
import mk.darko.demoExam.service.FurnitureService;

@RestController
public class FurnitureController {

	@Autowired
	private FurnitureService service;

	@PostMapping
	public void post(@RequestBody List<Furniture> furnitures) {
		service.getSum(furnitures);
	}
	
	@GetMapping
	public String getMostExpensiveFurniture() {
		return service.getMostExpensiveFurniture();
	}
}
