package mk.darko.demoExam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import mk.darko.demoExam.model.Furniture;

@Service
public class FurnitureServiceImpl implements FurnitureService {

	final int woodPrice = 9;
	final int glassPrice = 11;
	final int plasticPrice = 4;

	@Override
	public void getSum(List<Furniture> furniture) {

		int allCupboardsCost = 0;
		int allChairsCost = 0;
		int allTablesCost = 0;

		for (Furniture f : furniture) {

			if (f.getType().equals("cupboard")) {
				allCupboardsCost += f.getWood() * woodPrice;
			}

			if (f.getType().equals("chair")) {
				allChairsCost += f.getWood() * woodPrice;
				allChairsCost += f.getGlass() * glassPrice;
			}

			if (f.getType().equals("table")) {
				allTablesCost += f.getWood() * woodPrice;
				allTablesCost += f.getGlass() * glassPrice;
				allTablesCost += f.getPlastic() * plasticPrice;
			}
		}
	}

	public String mostExpensiveFurniture;

	public String getMostExpensiveFurniture() {

		return mostExpensiveFurniture;
	}

	private int findMax(int allCupboardsCost, int allChairsCost, int allTablesCost) {

		int max = allCupboardsCost;
		mostExpensiveFurniture.equals("cupboard");

		if (allChairsCost > max) {
			allChairsCost = max;
			mostExpensiveFurniture.equals("chair");

		}

		if (allTablesCost > max) {
			allTablesCost = max;
			mostExpensiveFurniture.equals("table");
		}
		return max;

	}
}
