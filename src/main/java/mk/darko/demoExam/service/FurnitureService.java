package mk.darko.demoExam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import mk.darko.demoExam.model.Furniture;


@Service
public interface FurnitureService {

	public void getSum(List<Furniture> furniture);
	
	public String getMostExpensiveFurniture();
}
