package mk.darko.demoExam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Furniture {
	
	private String type;
	private int wood;
	private int glass;
	private int plastic;

}
